<?php
    $wpfp_before = "";
    echo "<div class='wpfp-span greatfav'>";
    if (!empty($user)) {
        if (wpfp_is_user_favlist_public($user)) {
            $wpfp_before = "$user's Favorite Posts.";
        } else {
            $wpfp_before = "$user's list is not public.";
        }
    }

    if ($wpfp_before):
        echo '<div class="wpfp-page-before">'.$wpfp_before.'</div>';
    endif;

    if ($favorite_post_ids) {

        echo '<div class="favProWarpper">
                <div class="shareAndPrint">
                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                    <a href="#" onclick="printDiv(printMe)"><i class="fa fa-print" aria-hidden="true"></i></a>
                </div>
                <div class="row statiContent">
            <div class="col-lg-12">
                <h2>MY FAVORITE PRODUCTS </h2>
                <p>Our product catalog is home to thousands of products. 
                Collect your favorites and save them here to help you select the right product for your home.</p>
            </div>
        </div>';
		$favorite_post_ids = array_reverse($favorite_post_ids);
        $post_per_page = wpfp_get_option("post_per_page");
        $page = intval(get_query_var('paged'));

        $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

        foreach($post_array as $posttype){

        $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
        // custom post type support can easily be added with a line of code like below.
       //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
       $qry['post_type'] = array($posttype);
       
       query_posts($qry);

       $post_type_name = array_search($posttype,$brandmapping);
        
        echo '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
        if ( have_posts() ) :
            echo '<h3>'.$post_type_name.'</h3>';
        endif;
        while ( have_posts() ) : the_post();
           
            echo '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            echo '<div class="fl-post-grid-image prod_like_wrap">';

            $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');

            $sku = get_post_meta(get_the_ID(),'sku',true);
            $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);

            echo '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';

            echo '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                roomvo_script_integration($manufacturer,$sku,get_the_ID());
               echo '<a class="button fl-button" href="#">ADD A NOTE</a>'; 
                wpfp_remove_favorite_link(get_the_ID());
            echo '</div> </div>';
            echo ' <div class="fl-post-grid-text product-grid btn-grey">';
            echo '<h4><span>'.get_the_title().'</span></h4>';
            echo '</div>';

            //wpfp_remove_favorite_link(get_the_ID());
            echo "</div></div>";
        endwhile;
        echo "</div></div>";
    

    }
        

        echo '<div class="navigation">';
            if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
            <div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'buddypress' ) ) ?></div>
            <div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'buddypress' ) ) ?></div>
            <?php }
        echo '</div> </div>';

        wp_reset_query();
    } else {
        $wpfp_options = wpfp_get_options();
        echo "<ul><li>";
        echo $wpfp_options['favorites_empty'];
        echo "</li></ul>";
    }

    echo '<p>'.wpfp_clear_list_link().'</p>';
    echo "</div>";
    wpfp_cookie_warning();
